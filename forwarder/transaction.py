import time
import requests
import logging
import re


log = logging.getLogger(__name__)

class Transaction(object):
    API_KEY_REPLACEMENT = re.compile("api_key=*\\w+(\\w{5})")
    RETRY_INTERVAL = 5 # 5 second interval
    MAX_RETRY_INTERVAL = 90 # wait for a maximum of 90s to retry a transaction

    def __init__(self, payload, domain, endpoint, headers):
        self.payload = payload
        self.domain = domain
        self.endpoint = endpoint
        self.headers = headers
        self.created_at = time.time()
        self.nb_try = 0
        self.next_flush = None

    def get_endpoint(self):
        return self.API_KEY_REPLACEMENT.sub("api_key=***************************\\1", self.endpoint)

    def process(self):
        self.nb_try += 1

        url = self.domain + self.endpoint
        resp = requests.post(url, self.payload, headers=self.headers)

        if resp.status_code in (400, 404, 413):
            log.error("Error code %d received while sending transaction to %s: %s, dropping it",
                    resp.status_code, url, str(resp.text))
        elif resp.status_code == 403:
            log.error("API Key invalid, dropping transaction for %s", url)
        elif resp.status_code >= 400:
            log.error("error %q while sending transaction to %q, rescheduling it", resp.status_code, url)
            return False
        return True

    def reschedule(self):
        interval = min(self.nb_try * self.RETRY_INTERVAL, self.MAX_RETRY_INTERVAL)
        self.next_flush = time.time() + interval
