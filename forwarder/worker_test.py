import requests_mock
import Queue
import time

from worker import Worker, RetryWorker
from transaction import Transaction

@requests_mock.mock()
def test_worker_process_transactions(m):
    input_queue = Queue.Queue(2)
    retry_queue = Queue.Queue(2)
    w = Worker(input_queue, retry_queue)

    t_success = Transaction("data", "https://datadog.com", "/success", None)
    t_error = Transaction("data", "https://datadog.com", "/error", None)
    m.post("https://datadog.com/success", status_code=200)
    m.post("https://datadog.com/error", status_code=402)

    input_queue.put(t_success)
    input_queue.put(t_error)

    # process 2 transactions
    w.__process_transactions__()
    w.__process_transactions__()

    assert input_queue.empty()
    assert not retry_queue.empty()

    assert t_success.nb_try == 1
    assert t_error.nb_try == 1
    assert t_error.next_flush is not None

    retry_item = retry_queue.get()
    assert t_error == retry_item

def test_worker_stop():
    input_queue = Queue.Queue()
    retry_queue = Queue.Queue()
    w = Worker(input_queue, retry_queue)
    w.start()

    w.stop()
    w.join(2)
    assert not w.isAlive()

def test_retry_worker_flush():
    input_queue = Queue.Queue(1)
    retry_queue = Queue.Queue(1)
    w = RetryWorker(input_queue, retry_queue)

    t_ready = Transaction("data", "https://datadog.com", "/success", None)
    t_ready.next_flush = time.time() - 10
    w.transactions.append(t_ready)

    t_not_ready = Transaction("data", "https://datadog.com", "/success", None)
    t_not_ready.next_flush = time.time() + 1000
    w.transactions.append(t_not_ready)

    w.__flush_transactions__()
    assert len(w.transactions) == 1
    assert w.transactions[0] == t_not_ready

    try:
        t = input_queue.get_nowait()
    except Exception:
        # we should not fail to get a transaction
        raise Exception("input_queue should not be empty")
    assert t == t_ready

def test_retry_worker_process_transaction():
    input_queue = Queue.Queue(2)
    retry_queue = Queue.Queue(2)

    w = RetryWorker(input_queue, retry_queue, flush_interval=1)

    # test pulling 1 transaction without flushing
    t1 = Transaction("data", "https://datadog.com", "/success", None)
    t1.next_flush = time.time()
    retry_queue.put(t1)

    base_flush_time = time.time() + 10
    timeout, flush_time = w.__process_transactions__(10, base_flush_time)
    end = time.time()

    assert timeout <= 10
    # timeout should have been updated to reflect the time spend waiting for a new transaction
    assert timeout >= 10 - (base_flush_time - end)

    assert flush_time == base_flush_time
    assert retry_queue.qsize() == 0
    assert len(w.transactions) == 1
    assert w.transactions[0] == t1

    # now test with flush
    start = time.time()
    timeout, flush_time = w.__process_transactions__(0.1, 0)
    end = time.time()

    assert len(w.transactions) == 0
    try:
        t = input_queue.get(True, 1)
    except Queue.Empty:
        raise Exception("input_queue should not be empty")
    assert t == t1

    assert timeout == 1
    assert flush_time >= start + 1
    assert flush_time <= end + 1

def test_retryworker_stop():
    input_queue = Queue.Queue()
    retry_queue = Queue.Queue()
    w = RetryWorker(input_queue, retry_queue)
    w.start()

    w.stop()
    w.join(2)
    assert not w.isAlive()
