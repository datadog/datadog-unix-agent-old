from threading import Thread, Event
import Queue
import time
import logging


log = logging.getLogger(__name__)

class Worker(Thread):

    def __init__(self, input_queue, retry_queue):
        super(Worker, self).__init__()
        self.input_queue = input_queue
        self.retry_queue = retry_queue
        self.exit = Event()

    def stop(self):
        self.exit.set()

    def __process_transactions__(self):
        # blocking for 1 seconds so we can check the exit condition
        try:
            t = self.input_queue.get(True, 1)
        except Queue.Empty:
            return

        success = t.process()
        if not success:
            t.reschedule()
            try:
                self.retry_queue.put_nowait(t)
            except Queue.Full as e:
                log.errorf("Could not retry transaction to '%s', queue is full (dropping it): %s", t.get_endpoint(), e)

    def run(self):
        while not self.exit.is_set():
            self.__process_transactions__()

class RetryWorker(Worker):

    DEFAULT_FLUSH_INTERVAL = 1 # seconds

    def __init__(self, input_queue, retry_queue, flush_interval=0):
        super(RetryWorker, self).__init__(input_queue, retry_queue)
        self.transactions = []
        self.flush_interval = flush_interval if flush_interval else self.DEFAULT_FLUSH_INTERVAL

    def __flush_transactions__(self):
        if not self.transactions:
            return

        self.transactions.sort(key=lambda t: t.created_at, reverse=True)
        now = time.time()

        keep = []
        for t in self.transactions:
            if t.next_flush and t.next_flush > now:
                keep.append(t)
                continue

            try:
                self.input_queue.put_nowait(t)
            except Queue.Full:
                log.error("Can't retry connection, input queue is full: dropping transaction")

        self.transactions = keep

    def __process_transactions__(self, timeout, flush_time):
        try:
            t = self.retry_queue.get(True, timeout)
        except Queue.Empty:
            pass
        else:
            self.transactions.append(t)

        if flush_time <= time.time():
            self.__flush_transactions__()
            # reset timeout and set the next flush time
            timeout = self.flush_interval
            flush_time = time.time() + timeout
        else:
            # update timeout
            timeout = flush_time - time.time()

        return timeout, flush_time

    def run(self):
        timeout = self.flush_interval
        flush_time = time.time() + timeout

        while not self.exit.is_set():
            timeout, flush_time = self.__process_transactions__(timeout, flush_time)
